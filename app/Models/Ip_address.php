<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ip_address extends Model
{
    use HasFactory;

   protected $fillable = [
       'ip_address',
       'comment',
       'status',
       'created_at',
       'updated_at'
   ];
   
   protected $casts = [
    'updated_at' => 'datetime:d-M-Y H:i',
   ];
}
