<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Validator;

use App\Models\User;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);
        
        if ($validator->fails()) {
            $messages = $validator->errors();
            $response = [ 'message' => 'Login error.' ];
            foreach ($messages->all() as $message) {
                $response['error'][] = $message;
            }
            return response()->json($response, 200);
        }

        if (!Auth::attempt($request->only('email', 'password'))) {
            $response = [
                'message' => '',
                'error' => ['Invalid login credentials.']
            ];
            return response()->json($response, 200);
        }

        $user = User::where('email', $request['email'])->firstOrFail();
        $response = [            
            'message' => 'Successfully logged-in.',
            'auth_token_type' => 'Bearer',
            'auth_token' => $user->createToken('auth_token')->plainTextToken,
            'user' => $request->user()
        ];
        return response()->json($response, 200);
    }

    
    public function logout(Request $request)
    {
        $request->user()->currentAccessToken()->delete();
        $response = [            
            'message' => 'Successfully logged-out.'
        ];
        return response()->json($response, 200);
    }
}
