<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ip_address;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    public function index(){
        $response = [            
            'items' => Ip_address::all()->toArray(),
        ];
        return response()->json($response, 200);
    }

    public function edit(Request $response, $id){
        $response = [            
            'item' => Ip_address::find($id)->toArray(),
        ];
        return response()->json($response, 200);
    }

    public function save(Request $request){       

        if(!($request->id > 0)) { // for Add

            // Validation
            $validator = Validator::make($request->all(), [
                'ip_address' => 'required|ipv4'
            ]);
            if ($validator->fails()) {
                $messages = $validator->errors();
                $response = [ 'message' => 'Save error.' ];
                foreach ($messages->all() as $message) {
                    $response['error'][] = $message;
                }
                return response()->json($response, 200);
            }

            // Checks duplicate IP address entry
            if (Ip_address::where('ip_address', $request->ip_address)->get()->toArray()) {
                $response = [
                    'message' => '',
                    'error' => ['This IP address already exist.']
                ];
                return response()->json($response, 200);
            }
        }        

        DB::beginTransaction();
        try {
            if($request->id > 0){ // Edit
                $ipAddr = Ip_address::find($request->id);
                $ipAddr->updated_at = date('Y-m-d H:i:s');
            } else {              // Add
                $ipAddr = new Ip_address;
                $ipAddr->ip_address = $request->ip_address;
                $ipAddr->created_at = date('Y-m-d H:i:s');
                $ipAddr->updated_at = null;
            }
            $ipAddr->comment = $request->comment;
            $ipAddr->save();

            DB::commit(); // IF everything okay THEN commit
            $response = [            
                'message' => 'Successfully Saved.'
            ];
            return response()->json($response, 200);

        } catch (\Exception $e) {

            DB::rollback(); // ELSE throw exception and rollback
            $response = [            
                'message' => 'Failed to Saved.'
            ];
            return response()->json($response, 400);
        }

    }
    
    public function auditList(){
        $response = [            
            'items' => Ip_address::where('updated_at', '!=', null)->get()->toArray(),
        ];
        return response()->json($response, 200);
    }
}
