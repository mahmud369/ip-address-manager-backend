<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class DatabaseSeeder extends Seeder
{
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        DB::table('ip_addresses')->insert([ 'ip_address' => '223.85.96.141', 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('ip_addresses')->insert([ 'ip_address' => '8.8.8.8', 'created_at' => date('Y-m-d H:i:s') ]);


        $this->call([
            UserSeeder::class,
        ]);
    }
}
