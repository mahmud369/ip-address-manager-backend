<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['auth:sanctum']], function () {

    Route::post('/logout', [AuthController::class, 'logout']);

    // For testing the API
    Route::get('/user', function (Request $request) {
        return $request->user();
    });

    // Data interaction
    Route::get('/audit/list', [HomeController::class, 'auditList']);

    Route::get('/ip_addresses/list', [HomeController::class, 'index']);
    Route::get('/ip_addresses/edit/{id}', [HomeController::class, 'edit']);
    Route::post('/ip_addresses/save', [HomeController::class, 'save']);
});

// Non authenticated routes
Route::post('/login', [AuthController::class, 'login']);
