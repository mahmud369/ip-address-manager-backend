### Initial configurations

1. Create a MySql database named 'db_ip_addr_manager'.
2. Copy and rename '.env.example' file to '.env' file and put necessary mysql database configuration into it.
3. Run 'composer install' command in the CLI.
4. Next run 'php artisan migrate:fresh --seed' command in the CLI.
5. Next run 'php artisan key:generate' command in the CLI.


### Running the Project

6. Finally run 'php artisan serve --port=8090' command in the CLI to run the project.
